var searchData=
[
  ['advance_438',['advance',['../classpcg__detail_1_1engine.html#a12d3653cfe490a59f7ce55d028f1a7e0',1,'pcg_detail::engine::advance(itype state, itype delta, itype cur_mult, itype cur_plus)'],['../classpcg__detail_1_1engine.html#ad825cdb944e1a8c9e58e1b20dcbdf042',1,'pcg_detail::engine::advance(itype delta)'],['../classpcg__detail_1_1extended.html#ad917a2241597772e81668695c18ab2bb',1,'pcg_detail::extended::advance()']]],
  ['advance_5ftable_439',['advance_table',['../classpcg__detail_1_1extended.html#a5f496dd4c53e6543d9966f5c252074d9',1,'pcg_detail::extended::advance_table()'],['../classpcg__detail_1_1extended.html#a0d81db8641a6144e45738018a89f3850',1,'pcg_detail::extended::advance_table(state_type delta, bool isForwards=true)']]],
  ['average_5ffitness_440',['average_fitness',['../auxiliary__funs_8cpp.html#addc8f0ef3a052d32abb937843bd5ee03',1,'average_fitness(REAL_ x, const individual_s y):&#160;auxiliary_funs.cpp'],['../gaim_8h.html#a1177b09905836f47887034420641a2c0',1,'average_fitness(REAL_, const individual_s):&#160;auxiliary_funs.cpp']]]
];
