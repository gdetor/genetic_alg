var searchData=
[
  ['datainit_452',['datainit',['../classpcg__detail_1_1extended.html#abc0720b51a3a438758f73fd305686c42',1,'pcg_detail::extended']]],
  ['delta_453',['delta',['../mutation_8cpp.html#a98489f6a5793d78dee4e1425c9b21cea',1,'mutation.cpp']]],
  ['delta_5fmutation_454',['delta_mutation',['../mutation_8cpp.html#aae60272d405be8a81a20d55a44852a66',1,'delta_mutation(std::vector&lt; REAL_ &gt; genome, REAL_ mutation_rate=0.5, REAL_ variance=0.5):&#160;mutation.cpp'],['../gaim_8h.html#a576d08d180192ac01d9dac66547395d9',1,'delta_mutation(std::vector&lt; REAL_ &gt;, REAL_, REAL_):&#160;mutation.cpp']]],
  ['discard_455',['discard',['../classpcg__detail_1_1engine.html#ab47ab15e70c4ac7d284509642a4628a1',1,'pcg_detail::engine']]],
  ['discrete_5fcrossover_456',['discrete_crossover',['../crossover_8cpp.html#a630d2410511d517172af6e13ac545fdf',1,'discrete_crossover(std::vector&lt; REAL_ &gt; parent1, std::vector&lt; REAL_ &gt; parent2):&#160;crossover.cpp'],['../gaim_8h.html#a79addbdcfd7f50898f1cea5e007a0e21',1,'discrete_crossover(std::vector&lt; REAL_ &gt;, std::vector&lt; REAL_ &gt;):&#160;crossover.cpp']]],
  ['distance_457',['distance',['../classpcg__detail_1_1engine.html#aa58dc06aeb00200b73dbb890abc50fbf',1,'pcg_detail::engine::distance(itype cur_state, itype newstate, itype cur_mult, itype cur_plus, itype mask=~itype(0U))'],['../classpcg__detail_1_1engine.html#a00e9d352d7c83b5c5287d7b55b19b4bc',1,'pcg_detail::engine::distance(itype newstate, itype mask=~itype(0U)) const'],['../test__functions_8cpp.html#aeecd6e89e2a3202942f8bc969295608a',1,'distance():&#160;test_functions.cpp']]]
];
