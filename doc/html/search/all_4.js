var searchData=
[
  ['data_5f_40',['data_',['../classpcg__detail_1_1extended.html#ad5695148631932b277ec2cbac96efcb4',1,'pcg_detail::extended']]],
  ['datainit_41',['datainit',['../classpcg__detail_1_1extended.html#abc0720b51a3a438758f73fd305686c42',1,'pcg_detail::extended']]],
  ['default_42',['default',['../namespacepcg__detail.html#a7cefe974db0a1bb3a1fef33b171dfe60',1,'pcg_detail']]],
  ['default_5fincrement_43',['default_increment',['../structpcg__detail_1_1default__increment.html',1,'pcg_detail']]],
  ['default_5fincrement_3c_20itype_20_3e_44',['default_increment&lt; itype &gt;',['../structpcg__detail_1_1default__increment.html',1,'pcg_detail']]],
  ['default_5fmultiplier_45',['default_multiplier',['../structpcg__detail_1_1default__multiplier.html',1,'pcg_detail']]],
  ['default_5fmultiplier_3c_20itype_20_3e_46',['default_multiplier&lt; itype &gt;',['../structpcg__detail_1_1default__multiplier.html',1,'pcg_detail']]],
  ['delta_47',['delta',['../mutation_8cpp.html#a98489f6a5793d78dee4e1425c9b21cea',1,'mutation.cpp']]],
  ['delta_5fmutation_48',['delta_mutation',['../mutation_8cpp.html#aae60272d405be8a81a20d55a44852a66',1,'delta_mutation(std::vector&lt; REAL_ &gt; genome, REAL_ mutation_rate=0.5, REAL_ variance=0.5):&#160;mutation.cpp'],['../gaim_8h.html#a576d08d180192ac01d9dac66547395d9',1,'delta_mutation(std::vector&lt; REAL_ &gt;, REAL_, REAL_):&#160;mutation.cpp']]],
  ['discard_49',['discard',['../classpcg__detail_1_1engine.html#ab47ab15e70c4ac7d284509642a4628a1',1,'pcg_detail::engine']]],
  ['discrete_5fcrossover_50',['discrete_crossover',['../crossover_8cpp.html#a630d2410511d517172af6e13ac545fdf',1,'discrete_crossover(std::vector&lt; REAL_ &gt; parent1, std::vector&lt; REAL_ &gt; parent2):&#160;crossover.cpp'],['../gaim_8h.html#a79addbdcfd7f50898f1cea5e007a0e21',1,'discrete_crossover(std::vector&lt; REAL_ &gt;, std::vector&lt; REAL_ &gt;):&#160;crossover.cpp']]],
  ['distance_51',['distance',['../classpcg__detail_1_1engine.html#aa58dc06aeb00200b73dbb890abc50fbf',1,'pcg_detail::engine::distance(itype cur_state, itype newstate, itype cur_mult, itype cur_plus, itype mask=~itype(0U))'],['../classpcg__detail_1_1engine.html#a00e9d352d7c83b5c5287d7b55b19b4bc',1,'pcg_detail::engine::distance(itype newstate, itype mask=~itype(0U)) const'],['../test__functions_8cpp.html#aeecd6e89e2a3202942f8bc969295608a',1,'distance():&#160;test_functions.cpp']]]
];
