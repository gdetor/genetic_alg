var classIM =
[
    [ "IM", "classIM.html#a7899c310524ff324c00a92e4ad3806c8", null ],
    [ "~IM", "classIM.html#a450bc003d188ce2f3ff72b83ce78abfb", null ],
    [ "evolve_island", "classIM.html#acc814e945302b43a24da8532ffeeb73b", null ],
    [ "move_immigrants", "classIM.html#a581f6fd3f339c88635e75c6f3a15f487", null ],
    [ "read_connectivity_graph", "classIM.html#a19a5a20a20afb85340511fad53f4a9d9", null ],
    [ "run_islands", "classIM.html#a2113da7f6cacf768990ddc8c5a374526", null ],
    [ "select_ind2migrate", "classIM.html#af2a2985e55a2624e07a7b2bd30e9bfe7", null ],
    [ "a", "classIM.html#a4fc85a7e303c58f74399744a58c4dfe4", null ],
    [ "adj_list", "classIM.html#a5f4e569a338e52929a0488c036165832", null ],
    [ "b", "classIM.html#a33dbe595c898c6a4205a4a3873a75167", null ],
    [ "generations", "classIM.html#aee2c2c334f5e95c2c7c1b8a3db811410", null ],
    [ "island", "classIM.html#abbd9daac5118d5a6a3a0f1618bb69e71", null ],
    [ "migration_interval", "classIM.html#a2a4461a34fa9de6d4ea7c0dbe5653f55", null ],
    [ "migration_steps", "classIM.html#a7fb80afa381a603f0ba7ae2ed14749f5", null ],
    [ "mtx", "classIM.html#abaa9967120da8d0fe168ebcb60af2c54", null ],
    [ "num_immigrants", "classIM.html#a63b324a454c4c57d5438b89f0bfec051", null ],
    [ "num_islands", "classIM.html#a0544287f3561fb2a6288ab3ee0215d4a", null ]
];