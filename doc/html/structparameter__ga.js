var structparameter__ga =
[
    [ "a", "structparameter__ga.html#ad3dab59a58e3228c4de7630de91fa0d6", null ],
    [ "b", "structparameter__ga.html#a71e315dc7bc0c19cb2edf7c632aeced6", null ],
    [ "clipping_fname", "structparameter__ga.html#a102919dfdea00241442aa4d2d9a8c799", null ],
    [ "generations", "structparameter__ga.html#a12329a6d475e02fd715889728e504a27", null ],
    [ "genome_size", "structparameter__ga.html#a0a558f17babc2e8a47b181d7d7b7920d", null ],
    [ "num_offsprings", "structparameter__ga.html#abaa5369cab51b8d7832e07897ae25a5e", null ],
    [ "num_replacement", "structparameter__ga.html#aaf0d11846c4c999777935576c8ded7ef", null ],
    [ "population_size", "structparameter__ga.html#a4b89bdb4d13ba3dd51c7e08ea6cceb79", null ],
    [ "runs", "structparameter__ga.html#a158967a149f2685f612977302ea1266b", null ],
    [ "universal_clipping", "structparameter__ga.html#a4b768d38f1139a462d7224e57ffa629f", null ]
];