#!/usr/bin/env bash

# Installation script for the GAIM software 
# Copyright (C) 2019  Georgios Detorakis (gdetor@protonmail.com)
#                     Andrew Burton (ajburton@uci.edu)
#
# This program is free software; you can redistribute it and/or 
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

echo "========================================"
echo "Detecting the OS distro ..."
echo "========================================"
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    OS=Suse
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    OS=Redhat
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

debs="g++ make libconfig++9v5 libconfig++-dev openmpi-common openmpi-bin
openmpi-doc libopenmpi2 libopenmpi-dev libpthread-stubs0-dev"

rpms="g++ make openmpi libconfig libconfig++-devel libpthread-stubs"

arch="base-devel gcc make libconfig openmpi"

echo "========================================"
echo "Installing dependencies ... "
echo "========================================"
if [ "$1" != "" ]; then
    n=$1
else
    n=1
fi

if [ "$2" != "" ]; then
    tests=$2
else
    tests=0
fi

if [ "$3" != "" ]; then
    lib=$3
else
    lib=0
fi

flag=0
if [[ $OS == "Ubuntu" ]] || [[ $OS == "Debian" ]]; then
    sudo apt install -y $debs
    flag=1
elif [[ $OS == "Arch Linux" ]]; then
    sudo pacman -S $arch
    flag=1
elif [[ $OS == "Fedora" ]] || [[ $OS == "Redhat" ]]; then
    sudo dnf install $rpms
    flag=1
else
    echo "Distro not found!"
    exit -1
fi

if [[ $flag == 1 ]]; then
    echo "========================================"
    echo "Now compiling the standalone project ..."
    echo "========================================"
    mkdir -p build
    mkdir -p bin
    make clean
    make -j $n
fi

if [[ $flag == 1 ]] && [[ $lib == 1 ]]; then
    echo "========================================"
    echo "Now compiling the project as dynamic lib ..."
    echo "========================================"
    make clean
    make -j $n library
fi

if [[ $flag == 1 ]] && [[ $tests == 1 ]]; then
    echo "========================================"
    echo "Now compiling the tests ..."
    echo "========================================"
    make clean
    make -j $n tests
fi
